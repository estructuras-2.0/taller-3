from taller3 import indices_invertidos  # Importamos la función o código de taller3.py

# Palabra a buscar
palabra_prueba = "estafa"

# Ejecutamos la función taller3 con la palabra de prueba
resultado = indices_invertidos(palabra_prueba)

# Imprimimos el resultado
if resultado:
    print(f"Se encontraron resultados para '{palabra_prueba}':")
    print(resultado)
else:
    print(f"No se encontraron resultados para '{palabra_prueba}'")

