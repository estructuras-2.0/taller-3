from fastapi import FastAPI
import mook

app = FastAPI()

cache = {}

@app.get('/')
def root():
    return {"Servicio": "Estructuras de datos"}

@app.post('/indices-invertidos/{palabra}')
def indices_invertidos(palabra: str):
    if palabra in cache:
        return cache[palabra]

    result = {
        "decreto": "1234",
        "tutelas": []
    }

    for indice, tutela_info in enumerate(mook.tutela):
        if palabra.lower() in tutela_info["resumen"].lower():
            result["tutelas"].append(tutela_info)

    cache[palabra] = result
    return result
